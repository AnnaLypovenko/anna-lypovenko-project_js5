function createNewUser () {
    let firstName =  prompt ("Pls enter  your Name");
    let lastName =  prompt ("Pls enter  your last Name");
    let birthDay = prompt ("Please enter your date of birth", "dd.mm.yyyy");
    let dateParts = birthDay.split (".");
    let birthDayDate = new Date (Number(dateParts[2]), Number(dateParts[1]), Number(dateParts[0]));

    let newUser = {
        firstUserName: firstName,
        lastUserName: lastName,
        birthDay: birthDayDate,
        getLogin: function () {
            let login = this.firstUserName.toLowerCase()[0] + this.lastUserName.toLowerCase();
            return login;
        },
        getAge: function () {
                let userAge = new Date().getFullYear()- this.birthDay.getFullYear();
                return userAge;
        },
        getPassword: function () {
            let password = this.firstUserName.toUpperCase()[0] + this.lastUserName.toLowerCase() + this.birthDay.getFullYear();
            return password;
        }
    };
    return newUser;
}
let user = createNewUser();
let userLogin = user.getLogin();
let userAge = user.getAge();
let password = user.getPassword();
console.log (userLogin);
console.log (userAge);
console.log (password);